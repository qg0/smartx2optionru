#!/usr/bin/env python
import pandas as pd
import json
import datetime
import sys
import os


def drop_columns(df, columns_for_drop):
    for column in columns_for_drop:
        if column in df.columns:
            df.drop(column, axis=1, inplace=True)
    return df


def create_portfolio(input_file, output_file):
    df = pd.read_excel(input_file, decimal=',')

    df = df.rename(columns={
        'К-во со знаком(лоты)': 'qty',
        'К-во со знаком(штуки)': 'qty',
        'Текущие*позиции*': 'qty',
        'Текущие': 'qty',
        'Инструмент': 'code',
        'Тикер': 'ticker',
        'Крат. Наименование': 'short_code',
        'Портфель': 'portfolio',
        'Счёт': 'portfolio',
        'Средняя цена': 'open',
        'Ср.взв.цена': 'open',
        'Ср.взв.': 'open',
        'Направление': 'direction',
        'Торговая площадка': 'section',
        'К-во(лоты)': 'qty_unsigned',
        'К-во(штуки)': 'qty_unsigned',
        'Позиция': 'direction',
        'Тип инструмента': 'type_rus',
        'Площадка': 'section',
    })
    df = df.dropna()

    df2 = pd.DataFrame()

    if not df.filter(regex="Текущие*позиции*", axis=1).columns.empty:
        df2['qty'] = df.filter(regex="Текущие*позиции*", axis=1)
        df = pd.concat([df, df2], axis=1)
        
    columns_for_drop = [
        'Прибыль/Убыток, валюта',
        'Прибыль/Убыток %',
        'План. К-во(лоты)',
        'Величина',
        'Цена посл. сделки',
        'qty_unsigned',
        'direction',
        'В заявках на покупку',
        'В заявках на продажу',
        'Текущаяцена',
        'Нереализ.прибыль',
        'section',
        'Текущиепозиции**)',
        'Торговая площадка',
        'Поставка/расчеты',
        'Вход. К-во(лоты)',
        'type_rus',
        'В заявках',
        'В заявках.1',
    ]

    df = drop_columns(df, columns_for_drop)

    df['type'] = 'option'

    if 'type_rus' in df.columns:
        df.loc[(df['type_rus'] == 'Опционы'), 'type'] = 'option'
        df.loc[(df['type_rus'] == 'Фьючерсы'), 'type'] = 'futures'
        df.loc[(df['type_rus'] == 'Акции'), 'type'] = 'actions'
        df.loc[(df['type_rus'] == 'Облигации'), 'type'] = 'bonds'

    if 'type_rus' in df.columns and 'section' in df.columns:
        df.loc[(df['type_rus'] == 'Опционы') & (df['section'] == 'Срочный рынок MOEX'), 'type'] = 'option'
        df.loc[(df['type_rus'] == 'Фьючерсы') & (df['section'] == 'Срочный рынок MOEX'), 'type'] = 'futures'

    if 'qty' not in df.columns:
        if 'direction' in df.columns and 'qty_unsigned' in df.columns:
            df.loc[df['direction'] == 'Long', 'qty'] = df['qty_unsigned']
            df.loc[df['direction'] == 'Short', 'qty'] = -df['qty_unsigned']
        else:
            sys.exit('Нет полей: "Направление", ("К-во(лоты)" или "К-во(штуки)"')
        sys.exit('Нет полей: "К-во со знаком(лоты)" или "К-во со знаком(штуки)"')

    if 'ticker' in df.columns:
        df.loc[df['ticker'].astype(str).str.match(r'(?!([a-zA-Z]+)[0-9]+[a-zA-Z]{2,4}[0-9]+)'), 'type'] = 'futures'

        df['base'] = df['ticker'].str.extract(r'([a-zA-Z]{2})')
        df['code'] = df['ticker']
    else:
        if 'code' in df.columns:
            df.loc[df['code'].astype(str).str.endswith('_FT'), 'type'] = 'futures'

            df['base'] = df['code'].str.extract(r'(?P<base>[a-zA-Z]*)')
        else:
            sys.exit('Нет полей: "Тикер" или "Инструмент"')

    if 'section' in df.columns:
        df.loc[df['section'] == 'EQ', 'type'] = 'shares'
        df.loc[df['section'] == 'Фондовый рынок', 'type'] = 'shares'
    else:
        if 'ticker' in df.columns:
            df.loc[~df['ticker'].astype(str).str.contains('\\d', regex=True), 'type'] = 'shares'
        if 'code' in df.columns:
            df.loc[~df['code'].astype(str).str.contains('\\d', regex=True), 'type'] = 'shares'

    df = drop_columns(df, columns_for_drop)

    short_and_long_prefixes = {'RI': 'RTS'}
    df['base'].replace(to_replace=short_and_long_prefixes, inplace=True)

    if 'portfolio' not in df.columns:
        df['portfolio'] = df['base']

    portfolios = df['portfolio'].unique()
    bases = df['base'].unique()

    portfolios_out = ''
    for portfolio in portfolios:
        for base in bases:
            rows = df[(df['portfolio'] == portfolio) & (df['base'] == base)]
            rows_out = []
            for index, row in rows.iterrows():

                if type(row['open']) == str:
                    row['open'] = row['open'].replace(',', '')

                row_out = {
                    'type': row['type'],
                    'open': float(row['open']),
                    'code': row['code'],
                    'qty': int(row['qty']),
                }

                rows_out.append(row_out)

            if rows_out:
                base_out = {
                    'name': portfolio,
                    'filter': base,
                    'description': portfolio + ' ' + base + ' ' + datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                                   + ' ' + input_file,
                    'portfolio': rows_out
                }

                portfolios_out = portfolios_out + json.dumps(base_out, indent=2) + ',\r\n'

    f = open(output_file, "w")
    print(portfolios_out, file=f)
    f.close()

    print(portfolios_out)


def get_default_input_file():
    directories = ['./', 'Новая папка', './smartx', 'New Folder', 'itinvest', './files']
    file_names = ['smartx', 'Лист Microsoft Excel', 'Untitled', 'Без имени', 'Book', 'Spreadsheet',
                  'Электронная таблица Calligra Sheets']
    suffixes = ['', '0', '1', '2', '3', '4', ' 0', ' 1', ' 2', ' 3', ' 4']
    extensions = ['xlsx', 'ods', 'xls', 'xlsm', 'xlsb', 'odf', 'odt']

    file_path = ''

    try:
        for directory in directories:
            for file_name in file_names:
                for suffix in suffixes:
                    for extension in extensions:
                        file_path = directory + file_name + suffix + '.' + extension

                        if os.path.exists(file_path):
                            raise StopIteration

    except StopIteration:
        return file_path


if __name__ == '__main__':
    input_file = ''
    output_file = 'option.ru.txt'

    if len(sys.argv) == 1:
        input_file = get_default_input_file()

        if not input_file:
            sys.exit('Не указан входной файл')

    if len(sys.argv) > 1:
        input_file = sys.argv[1]
    if len(sys.argv) > 2:
        output_file = sys.argv[2]

    create_portfolio(input_file, output_file)
